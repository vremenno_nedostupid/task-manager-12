package ru.fedun.tm.service;

import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.model.Project;

import java.util.List;

public class ProjectService implements ICrudService<Project> {

    private final ICrudRepository<Project> projectRepository;

    public ProjectService(ICrudRepository<Project> projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(String title, String description) {
        final Project project = new Project();
        project.setTitle(title);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void create(String title) {
        final Project project = new Project();
        project.setTitle(title);
        projectRepository.add(project);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project getOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project getOneByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project getOneByTitle(String title) {
        if (title == null || title.isEmpty()) return null;
        return projectRepository.findOneByTitle(title);
    }

    @Override
    public Project updateById(String id, String title, String description) {
        if (id == null || id.isEmpty()) return null;
        if (title == null || title.isEmpty()) return null;
        final Project project = projectRepository.findOneById(id);
        if (project == null) return null;
        project.setId(id);
        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(Integer index, String title, String description) {
        if (index == null || index < 0) return null;
        if (title == null || title.isEmpty()) return null;
        final Project project = projectRepository.findOneByIndex(index);
        if (project == null) return null;
        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeOneByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByTitle(String title) {
        if (title == null || title.isEmpty()) return null;
        return projectRepository.findOneByTitle(title);
    }

}
