package ru.fedun.tm.api.service;

import java.util.List;

public interface ICrudService<T> {

    void create(String title);

    void create(String title, String description);

    List<T> findAll();

    void clear();

    T getOneById(String id);

    T getOneByIndex(Integer index);

    T getOneByTitle(String title);

    T updateById(String id, String title, String description);

    T updateByIndex(Integer index, String title, String description);

    T removeOneById(String id);

    T removeOneByIndex(Integer index);

    T removeOneByTitle(String title);

}
