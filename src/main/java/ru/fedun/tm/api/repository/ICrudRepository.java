package ru.fedun.tm.api.repository;

import java.util.List;

public interface ICrudRepository<T> {

    List<T> findAll();

    void clear();

    void add(T o);

    void remove(T o);

    T findOneById(String id);

    T findOneByIndex(Integer index);

    T findOneByTitle(String title);

    T removeOneById(String id);

    T removeOneByIndex(Integer index);

    T removeOneByTitle(String title);

}
