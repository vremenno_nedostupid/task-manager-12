package ru.fedun.tm.repository;

import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements ICrudRepository<Project> {

    private final List<Project> projects = new ArrayList<>();

    public void add(Project project) {
        projects.add(project);
    }

    public void remove(Project project) {
        projects.remove(project);
    }

    public List<Project> findAll() {
        return projects;
    }

    public void clear() {
        projects.clear();
    }

    @Override
    public Project findOneById(String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(Integer index) {
        return projects.get(index);
    }

    @Override
    public Project findOneByTitle(String title) {
        for (final Project project : projects) {
            if (title.equals(project.getTitle())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeOneByIndex(Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeOneByTitle(String title) {
        final Project project = findOneByTitle(title);
        if (project == null) return null;
        remove(project);
        return project;
    }

}
