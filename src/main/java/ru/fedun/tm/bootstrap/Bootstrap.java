package ru.fedun.tm.bootstrap;

import ru.fedun.tm.api.controller.ICommandController;
import ru.fedun.tm.api.controller.ICrudController;
import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.api.service.ICommandService;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.constant.ArgumentConst;
import ru.fedun.tm.controller.CommandController;
import ru.fedun.tm.controller.ProjectController;
import ru.fedun.tm.controller.TaskController;
import ru.fedun.tm.model.Project;
import ru.fedun.tm.model.Task;
import ru.fedun.tm.repository.CommandRepository;
import ru.fedun.tm.repository.ProjectRepository;
import ru.fedun.tm.repository.TaskRepository;
import ru.fedun.tm.service.CommandService;
import ru.fedun.tm.service.ProjectService;
import ru.fedun.tm.service.TaskService;
import ru.fedun.tm.util.TerminalUtil;

import static ru.fedun.tm.constant.TerminalConst.*;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ICrudRepository<Task> taskRepository = new TaskRepository();

    private final ICrudService<Task> taskService = new TaskService(taskRepository);

    private final ICrudController<Task> taskController = new TaskController(taskService);

    private final ICrudRepository<Project> projectRepository = new ProjectRepository();

    private final ICrudService<Project> projectService = new ProjectService(projectRepository);

    private final ICrudController<Project> projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        commandController.displayWelcome();
        if (args.length != 0) {
            runWithArgs(args[0]);
        }
        runWithCommand();
    }

    private void runWithCommand() {
        while (true) {
            switch (TerminalUtil.nextLine()) {
                case VERSION:
                    commandController.displayVersion();
                    break;
                case ABOUT:
                    commandController.displayAbout();
                    break;
                case HELP:
                    commandController.displayHelp();
                    break;
                case INFO:
                    commandController.displayInfo();
                    break;
                case COMMANDS:
                    commandController.displayCommands();
                    break;
                case ARGUMENTS:
                    commandController.displayArgs();
                    break;
                case TASK_LIST:
                    taskController.showAll();
                    break;
                case TASK_CREATE:
                    taskController.create();
                    break;
                case TASK_CLEAR:
                    taskController.clear();
                    break;
                case PROJECT_LIST:
                    projectController.showAll();
                    break;
                case PROJECT_CREATE:
                    projectController.create();
                    break;
                case PROJECT_CLEAR:
                    projectController.clear();
                    break;
                case TASK_VIEW_BY_ID:
                    taskController.showOneById();
                    break;
                case TASK_VIEW_BY_INDEX:
                    taskController.showOneByIndex();
                    break;
                case TASK_VIEW_BY_TITLE:
                    taskController.showOneByTitle();
                    break;
                case TASK_UPDATE_BY_ID:
                    taskController.updateOneById();
                    break;
                case TASK_UPDATE_BY_INDEX:
                    taskController.updateOneByIndex();
                    break;
                case TASK_REMOVE_BY_ID:
                    taskController.removeOneById();
                    break;
                case TASK_REMOVE_BY_INDEX:
                    taskController.removeOneByIndex();
                    break;
                case TASK_REMOVE_BY_TITLE:
                    taskController.removeOneByTitle();
                    break;
                case PROJECT_VIEW_BY_ID:
                    projectController.showOneById();
                    break;
                case PROJECT_VIEW_BY_INDEX:
                    projectController.showOneByIndex();
                    break;
                case PROJECT_VIEW_BY_TITLE:
                    projectController.showOneByTitle();
                    break;
                case PROJECT_UPDATE_BY_ID:
                    projectController.updateOneById();
                    break;
                case PROJECT_UPDATE_BY_INDEX:
                    projectController.updateOneByIndex();
                    break;
                case PROJECT_REMOVE_BY_ID:
                    projectController.removeOneById();
                    break;
                case PROJECT_REMOVE_BY_INDEX:
                    projectController.removeOneByIndex();
                    break;
                case PROJECT_REMOVE_BY_TITLE:
                    projectController.removeOneByTitle();
                    break;
                case EXIT:
                    commandController.exit();
                default:
                    commandController.displayError();
            }
        }
    }

    private void runWithArgs(final String arg) {
        if (arg == null || arg.isEmpty()) {
            System.out.println("Empty command");
            return;
        }
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArgs();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default:
                commandController.displayError();
        }
    }

}
