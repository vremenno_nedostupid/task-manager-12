package ru.fedun.tm.controller;

import ru.fedun.tm.api.controller.ICrudController;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.model.Task;
import ru.fedun.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ICrudController<Task> {

    private final ICrudService<Task> taskService;

    public TaskController(final ICrudService<Task> taskService) {
        this.taskService = taskService;
    }

    public void showAll() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " +task);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
        System.out.println();
    }

    public void create() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        taskService.create(title, description);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void showOneById() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.getOneById(id);
        if (task == null) System.out.println("[FAIL]");
        else showTask(task);
    }

    @Override
    public void showOneByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.getOneByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else showTask(task);
        System.out.println();
    }

    @Override
    public void showOneByTitle() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        final Task task = taskService.getOneByTitle(title);
        if (task == null) System.out.println("[FAIL]");
        else showTask(task);
    }

    public void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("TITLE: " + task.getTitle());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    @Override
    public void updateOneByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt();
        final Task task = taskService.getOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
        }
        System.out.println("ENTER TITLE:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateByIndex(index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateOneById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.getOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
        }
        System.out.println("ENTER TITLE:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateById(id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeOneById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        final Task task = taskService.removeOneById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeOneByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt();
        if (index <= 0) {
            System.out.println("[FAIL]");
            return;
        }
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeOneByTitle() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        if (title == null || title.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        final Task task = taskService.removeOneByTitle(title);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }
}
