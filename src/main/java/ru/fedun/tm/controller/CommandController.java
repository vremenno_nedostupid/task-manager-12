package ru.fedun.tm.controller;

import ru.fedun.tm.api.controller.ICommandController;
import ru.fedun.tm.api.service.ICommandService;
import ru.fedun.tm.model.Command;
import ru.fedun.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void displayWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println();
    }

    public void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
        System.out.println();
    }

    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
        System.out.println();
    }

    public void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Alexander Fedun");
        System.out.println("sasha171998@gmail.com");
        System.out.println();
    }

    public void displayCommands() {
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
        System.out.println();
    }

    public void displayArgs() {
        final String[] args = commandService.getArgs();
        System.out.println(Arrays.toString(args));
    }

    public void displayInfo() {
        System.out.println("[INFO]");

        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + processors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory: " + usedMemoryFormat);

        System.out.println();
    }

    public void displayError() {
        System.out.println("Error: unknown command");
        System.out.println();
    }

    public void exit() {
        System.exit(0);
    }
}
